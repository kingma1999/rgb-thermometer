#include <avr/sleep.h>
#include <avr/interrupt.h>
#include "HSVtoRGB.hpp"
int R_pin = 4;
int switchPin = 3;
int G_pin = 1;
int B_pin = 0;
int i;
long StartMillis;
long CurrentMillis;

void setup() {
  pinMode(R_pin, OUTPUT);
  pinMode(G_pin, OUTPUT);
  pinMode(B_pin, OUTPUT);
  StartMillis = millis();
  pinMode(switchPin, INPUT);
  digitalWrite(switchPin, HIGH);
}

void sleep() {
    GIMSK |= _BV(PCIE);                     // Enable Pin Change Interrupts
    PCMSK |= _BV(PCINT3);                   // Use PB3 as interrupt pin
    ADCSRA &= ~_BV(ADEN);                   // ADC off
    set_sleep_mode(SLEEP_MODE_PWR_DOWN);    // replaces above statement

    sleep_enable();                         // Sets the Sleep Enable bit in the MCUCR Register (SE BIT)
    sei();                                  // Enable interrupts
    sleep_cpu();                            // sleep

    cli();                                  // Disable interrupts
    PCMSK &= ~_BV(PCINT3);                  // Turn off PB3 as interrupt pin
    sleep_disable();                        // Clear SE bit
    ADCSRA |= _BV(ADEN);                    // ADC on

    sei();                                  // Enable interrupts
    } // sleep

ISR(PCINT0_vect) {
    delay(10);
    StartMillis = millis();
    }

void loop(){
  for (i = 0; i <= 360;) {
    RGB Color = HSVtoRGB(i,1,1);
    analogWrite(R_pin, Color.r*255);
    analogWrite(G_pin, Color.g*255);
    analogWrite(B_pin, Color.b*255);
    delay(3);
    i = i + 1;
  }
  i = 0;
  CurrentMillis = millis();
  if (CurrentMillis - StartMillis >= 30000) {
    analogWrite(R_pin, 255);
    analogWrite(G_pin, 255);
    analogWrite(B_pin, 255);
    CurrentMillis = 0;
    sleep();
  }
}
