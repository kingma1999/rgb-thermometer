#include "HSVtoRGB.hpp"

RGB& operator+=(RGB& col, float val)
{
    col.r += val;
    col.g += val;
    col.b += val;

    return col;
}

float abs(float value)
{
    return value >= 0 ? value : -value;
}

float fmod(float x, float y)
{
    // The floating-point remainder of the division operation x/y calculated by this function is exactly the value x - n*y, where n is x/y with its fractional part truncated. 

    int n = x/y;
    return x - n*y;
}

RGB HSVtoRGB(float hue, float saturation, float value)
{
    hue = (int)hue % 360;
    if (saturation < 0 || saturation > 1 || value < 0 || value > 1)
        return {0,0,0};

    float chroma = value * saturation;

    float hue_prime = hue/60;

    float x = chroma * (1 - abs(fmod(hue_prime, 2) - 1));

    RGB intermediate;

    if (hue_prime <= 1)
        intermediate = {chroma, x, 0};
    else if (hue_prime <= 2)
        intermediate = {x, chroma, 0};
    else if (hue_prime <= 3)
        intermediate = {0, chroma, x};
    else if (hue_prime <= 4)
        intermediate = {0, x, chroma};
    else if (hue_prime <= 5)
        intermediate = {x, 0, chroma};
    else if (hue_prime <= 6)
        intermediate = {chroma, 0, x};

    float m = value - chroma;

    intermediate += m;

    return intermediate;
}
